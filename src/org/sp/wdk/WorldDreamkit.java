package org.sp.wdk;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.sp.wdk.db.YMLDB;
import org.sp.wdk.ui.*;
import org.sp.wdk.config.Config;
import org.sp.wdk.listeners.AccessListener;
import org.sp.wdk.listeners.PlayerListener;
import org.sp.wdk.listeners.PlayerUpdateTask;
import org.sp.wdk.log.L;
import org.sp.wdk.world.player.PlayerManager;
import org.sp.wdk.world.player.PlayerSyncTask;
import org.sp.wdk.world.regen.RegenManager;
import org.sp.wdk.world.regen.RegenTask;
import org.sp.wdk.world.zone.ZoneManager;
import org.sp.wdk.world.zone.ZoneRemoveTask;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 7:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class WorldDreamkit extends JavaPlugin {
    private ZoneManager zm;
    private PlayerManager pm;
    private YMLDB db;
    private RegenManager rm;
    private WorldEditPlugin we;


    @Override
    public void onEnable() {
        saveDefaultConfig();
        getConfig().options().copyDefaults(true);

        L.l = this.getLogger();
        Config.c = getConfig();

        we = (WorldEditPlugin) getServer().getPluginManager().getPlugin("WorldEdit");
        db = new YMLDB(this);
        zm = new ZoneManager();
        pm = new PlayerManager(this);
        rm = new RegenManager(this);

        getServer().getPluginManager().registerEvents(new AccessListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerListener(this), this);

        getCommand("claim").setExecutor(new ClaimDispatcher(this));
        getCommand("reclaim").setExecutor(new ReClaimDispatcher(this));

        getCommand("info").setExecutor(new InfoDispatcher(this));
        getCommand("remove").setExecutor(new RemoveDispatcher(this));
        getCommand("select").setExecutor(new SelectDispatcher(this));
        getCommand("zone").setExecutor(new ManageDispatcher(this));

        new PlayerUpdateTask(this).runTaskTimer(this, 0, 20);
        new ZoneRemoveTask(this).runTaskTimer(this, 0, 20); // @TODO: production value = 20*60
        new RegenTask(this).runTaskTimer(this, 0, 20); // @TODO: production value = ?
        new PlayerSyncTask(this).runTaskTimer(this, 0, 20*30);

        db.loadZonesMap(zm);
        //zm.push(new Zone(new Rect(0, 0, 0, 100, 200, 100), "5Shadow7Prince"));

        for (Player p : getServer().getOnlinePlayers()) {
            pm.push(p);
        }
    }

    @Override
    public void onDisable() {
        db.saveZonesMap(zm);
    }

    public PlayerManager getPm() {
        return pm;
    }

    public ZoneManager getZm() {
        return zm;
    }

    public RegenManager getRm() {
        return rm;
    }

    public WorldEditPlugin getWe() {
        return we;
    }
}
