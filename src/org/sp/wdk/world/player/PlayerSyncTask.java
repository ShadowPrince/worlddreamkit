package org.sp.wdk.world.player;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.log.L;

/**
 * User: sp
 * Date: 4/26/13
 * Time: 7:47 PM
 */
public class PlayerSyncTask extends BukkitRunnable {
    private WorldDreamkit wdk;

    public PlayerSyncTask(WorldDreamkit wdk) {
        this.wdk = wdk;
    }

    @Override
    public void run() {
        for (Player p : wdk.getServer().getOnlinePlayers()) {
            if (!wdk.getPm().inside(p)) {
                L.d(String.format("Player %s was not in manager! (%d)", p.getName(), System.currentTimeMillis()));
                wdk.getPm().push(p);
            }
        }
    }
}
