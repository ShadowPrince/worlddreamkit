package org.sp.wdk.world.player;

import org.sp.wdk.world.zone.Zone;
import org.bukkit.entity.Player;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 9:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class PlayerState {
    private Player player;
    private Zone zone;
    private long time;

    public PlayerState(Player player, Zone zone) {
        this.player = player;
        this.zone = zone;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player p) {
        this.player = p;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone z) {
        this.zone = z;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
