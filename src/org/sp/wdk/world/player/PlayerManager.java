package org.sp.wdk.world.player;

import org.bukkit.event.Event;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.listeners.event.PlayerEnterZoneEvent;
import org.sp.wdk.listeners.event.PlayerLeaveZoneEvent;
import org.sp.wdk.log.L;
import org.sp.wdk.world.zone.Zone;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 9:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class PlayerManager {
    private HashMap<String, PlayerState> players = new HashMap<String, PlayerState>();
    private WorldDreamkit wdk;

    public PlayerManager(WorldDreamkit wdk) {
        this.wdk = wdk;
    }

    public void push(Player player) {
        if (inside(player)) {
            L.d(String.format("Pushing player %s to manager failed: exits", player.getName()));
        } else {
            L.d(String.format("Pushing player %s to manager.", player.getName()));
            players.put(player.getName(), new PlayerState(player, wdk.getZm().find(player.getLocation())));
        }
    }

    public void eject(Player player) {
        L.d(String.format("Ejecting player %s from manager.", player.getName()));
        players.remove(player.getName());
    }

    public boolean inside(Player player) {
        return players.containsKey(player.getName());
    }

    public void update() {
        //L.d("Updating player manager...");
        for (Map.Entry<String, PlayerState> e : players.entrySet()) {
            Zone z = wdk.getZm().find(e.getValue().getPlayer().getLocation());
            if (z != null) {
                if (e.getValue().getZone() == null) { // player entered zone
                    wdk.getServer().getPluginManager().callEvent(new PlayerEnterZoneEvent(
                            new PlayerState(e.getValue().getPlayer(), z)
                    ));
                }
                //L.d(String.format("    Player %s in zone %s", e.getValue().getPlayer().getName(), z.getName()));
                e.getValue().setZone(z);
                e.getValue().setTime(System.currentTimeMillis());
            } else {
                if (e.getValue().getZone() != null) { // player leave zone
                    wdk.getServer().getPluginManager().callEvent(new PlayerLeaveZoneEvent(e.getValue()));
                }
                e.setValue(new PlayerState(
                    e.getValue().getPlayer(),
                    null
                ));
            }
        }
    }

    public Zone getZoneFor(Player p) {
        PlayerState ps = players.get(p.getName());
        if (ps != null)
            return ps.getZone();
        else
            return null;
    }
}
