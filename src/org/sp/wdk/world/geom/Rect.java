package org.sp.wdk.world.geom;

import org.bukkit.Location;
import org.bukkit.World;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 7:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class Rect {
    private int x1, y1, z1, x2, y2, z2;
    private World world;

    public Rect(World world, int x1, int y1, int z1, int x2, int y2, int z2) {
        this.x1 = x1;
        this.y1 = y1;
        this.z1 = z1;
        this.x2 = x2;
        this.y2 = y2;
        this.z2 = z2;
        this.world = world;
    }

    public Rect(World world, double x1, double y1, double z1, double x2, double y2, double z2) {
        this.x1 = (int) x1;
        this.y1 = (int) y1;
        this.z1 = (int) z1;
        this.x2 = (int) x2;
        this.y2 = (int) y2;
        this.z2 = (int) z2;
        this.world = world;
    }

    public Location getStartLocation() {
        return new Location(getWorld(), getX1(), getY1(), getZ1());
    }

    public Location getEndLocation() {
        return new Location(getWorld(), getX2(), getY2(), getZ2());
    }

    public boolean inside(Rect r) {

        return false;
    }

    public boolean contains(Point p) {
        return
                        p.getWorld() == getWorld() &&
                        getX1() <= p.getX() &&
                        getX2() >= p.getX() &&
                        getY1() <= p.getY() &&
                        getY2() >= p.getY() &&
                        getZ1() <= p.getZ() &&
                        getZ2() >= p.getZ()
        ;
    }

    public boolean intersects(Rect r) {
        return false;
    }

    public String toString() {
        return String.format("(%d %d %d)-(%d %d %d) %s",
                getX1(), getY1(), getZ1(),
                getX2(), getY2(), getZ2(),
                getWorld().getName()
                );
    }

    public int getZ2() {
        return z2;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getZ1() {
        return z1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public World getWorld() {
        return world;
    }

}
