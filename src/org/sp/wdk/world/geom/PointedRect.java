package org.sp.wdk.world.geom;

import org.bukkit.World;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 8:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class PointedRect extends Rect {
    private int id;

    public PointedRect(World world, int x1, int y1, int z1, int x2, int y2, int z2, int id) {
        super(world, x1, y1, z1, x2, y2, z2);
        this.id = id;
    }

    public PointedRect(Rect r, int id) {
        super(r.getWorld(), r.getX1(), r.getY1(), r.getZ1(), r.getX2(), r.getY2(), r.getZ2());
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
