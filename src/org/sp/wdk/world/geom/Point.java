package org.sp.wdk.world.geom;

import org.bukkit.Location;
import org.bukkit.World;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 7:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class Point {
    private int x, y, z;
    private World world;

    public Point(World world, int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.world = world;
    }

    public Point(Location l) {
        this.x = (int) l.getX();
        this.y = (int) l.getY();
        this.z = (int) l.getZ();
        this.world = l.getWorld();
    }

    public String toString() {
        return String.format("(%d %d %d)", getX(), getY(), getZ());
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public World getWorld() {
        return world;
    }
}
