package org.sp.wdk.world.regen;

import com.sk89q.worldedit.masks.Mask;
import com.sk89q.worldedit.regions.Region;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.world.geom.Rect;

import java.util.ArrayList;
import java.util.List;

/**
 * User: sp
 * Date: 4/18/13
 * Time: 5:41 AM
 */
public class RegenManager {
    private WorldDreamkit wdk;
    private List<Rect> regen = new ArrayList<Rect>();

    public RegenManager(WorldDreamkit wdk) {
        this.wdk = wdk;
    }

    public void add(Rect r) {
        regen.add(r);
    }

    public void regenFirst() {
        if (regen.size() > 0) {
            Rect r = regen.get(0);

            // @TODO: regen

            regen.remove(0);
        }
    }

}
