package org.sp.wdk.world.regen;

import org.bukkit.scheduler.BukkitRunnable;
import org.sp.wdk.WorldDreamkit;

/**
 * User: sp
 * Date: 4/18/13
 * Time: 5:40 AM
 */
public class RegenTask extends BukkitRunnable {
    private WorldDreamkit wdk;

    public RegenTask(WorldDreamkit wdk) {
        this.wdk = wdk;
    }

    @Override
    public void run() {
        wdk.getRm().regenFirst();
    }
}
