package org.sp.wdk.world.zone;

import org.bukkit.Location;
import org.sp.wdk.log.L;
import org.sp.wdk.world.geom.Point;
import org.sp.wdk.world.geom.PointedRect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 7:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class ZoneManager {
    private HashMap<Integer, Zone> zones = new HashMap<Integer, Zone>();
    private List<PointedRect> rects = new ArrayList<PointedRect>();
    private HashMap<String, Integer> names = new HashMap<String, Integer>();
    private int counter = 0;

    public ZoneManager() {

    }

    public Zone find(Point p) {
        //L.d(String.format("Searching at %s", p));
        for (PointedRect r : rects) {
            //L.d(String.format("Try %s", r));
            if (r.contains(p)) return zones.get(r.getId());
        }

        return null;
    }

    public Zone find(Location l) {
        return find(new Point(l));
    }

    public Zone find(String name) {
        Integer id = names.get(name);

        if (id == null)
            return null;
        else
            return get(id);
    }

    public void pushNamed(Zone z) {
        L.d(String.format("Pushing zone %s", z.getName()));
        rects.add(new PointedRect(z.getRect(), counter));
        names.put(z.getName(), counter);
        zones.put(counter, z);
    }

    public void push(Zone z) {
        counter++;

        z.setName(z.getMainOwner() + Integer.toString(counter));
        z.setId(counter);

        pushNamed(z);
    }

    public void push(Zone z, String name) {
        counter++;

        z.setName(name);
        z.setId(counter);

        pushNamed(z);
    }

    public Zone get(int id) {
        return zones.get(id);
    }

    public void remove(int id) {
        zones.remove(id);
    }


    public HashMap<Integer,Zone> getZones() {
        return zones;
    }
}

