package org.sp.wdk.world.zone;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.sp.wdk.world.geom.Rect;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 7:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class Zone {
    private Rect rect;
    private long remove = 0;
    private List<String> owners = new ArrayList<String>();
    private List<String> members = new ArrayList<String>();
    private String name;
    private int id;

    public Zone(Rect rect, String p) {
        this.rect = rect;
        owners.add(p);
    }

    public boolean ownedBy(Player p) {
        return owners.contains(p.getName());
    }

    public boolean ownedBy(String p) {
        return owners.contains(p);
    }

    public boolean isMember(Player p) {
        return members.contains(p.getName());
    }

    public boolean isMember(String p) {
        return members.contains(p);
    }

    public boolean accessibleBy(Player p) {
        return ownedBy(p) || isMember(p);
    }

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public void addOwner(Player p) {
        owners.add(p.getName());
    }

    public void removeOwner(Player p) {
        owners.remove(p.getName());
    }

    public void addOwner(String p) {
        owners.add(p);
    }

    public void removeOwner(String p) {
        owners.remove(p);
    }

    public void addMember(String p) {
        members.add(p);
    }

    public void removeMember(String p) {
        members.remove(p);
    }

    public String getMainOwner() {
        return owners.get(0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwners(List<String> owners) {
        this.owners = owners;
    }

    public List<String> getOwners() {
        return owners;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public List<String> getMembers() {
        return members;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getRemove() {
        return remove;
    }

    public void setRemove(long remove) {
        this.remove = remove;
    }
}
