package org.sp.wdk.world.zone;

import org.bukkit.scheduler.BukkitRunnable;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.log.L;

import java.util.Map;

/**
 * User: sp
 * Date: 4/18/13
 * Time: 5:31 AM
 */
public class ZoneRemoveTask extends BukkitRunnable {
    private WorldDreamkit wdk;

    public ZoneRemoveTask(WorldDreamkit wdk) {
        this.wdk = wdk;
    }

    @Override
    public void run() {
        for (Map.Entry<Integer, Zone> e : wdk.getZm().getZones().entrySet()) {
            if (e.getValue().getRemove() != 0 && System.currentTimeMillis() - e.getValue().getRemove() > 1000*60* 10) {
                L.d(String.format("Zone %s out of time and now removed. Zone will be regenerated shortly. ", e.getValue().getName()));
                wdk.getRm().add(e.getValue().getRect());
                wdk.getZm().remove(e.getValue().getId());
            }
        }
    }
}
