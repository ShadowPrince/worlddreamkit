package org.sp.wdk.log;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 9:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class L {
    public static Logger l;

    public static void log(Level lvl, String s) {
        l.log(lvl, s);
    }

    public static void l(String s) {
        L.log(Level.INFO, s);
    }

    public static void d(String s) {
        L.log(Level.SEVERE, s);
    }

    public static void e(String s) {
        L.log(Level.WARNING, s);
    }
}
