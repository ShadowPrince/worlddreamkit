package org.sp.wdk.db;

import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.world.zone.Zone;
import org.sp.wdk.world.zone.ZoneManager;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/18/13
 * Time: 3:50 AM
 * To change this template use File | Settings | File Templates.
 */
public interface DatabaseInterface {
    public void loadZonesMap(ZoneManager zm);
    public void saveZonesMap(ZoneManager zm);
}
