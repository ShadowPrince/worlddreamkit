package org.sp.wdk.db;

import org.bukkit.configuration.ConfigurationSection;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.config.Config;
import org.sp.wdk.log.L;
import org.sp.wdk.world.geom.Rect;
import org.sp.wdk.world.zone.Zone;
import org.sp.wdk.world.zone.ZoneManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: sp
 * Date: 4/18/13
 * Time: 4:27 AM
 */
public class YMLDB implements DatabaseInterface {
    private List<Exception> brokenData = new ArrayList<Exception>();
    private WorldDreamkit wdk;

    public YMLDB(WorldDreamkit wdk) {
        this.wdk = wdk;
    }

    @Override
    public void loadZonesMap(ZoneManager zm) {
        int i = 0;
        ConfigurationSection zones = Config.c.getConfigurationSection("zones");
        if (zones == null) {
            return;
        }

        for (Map.Entry<String, Object> e : zones.getValues(false).entrySet()) {
            try {
                i++;
                String name = e.getKey();
                ConfigurationSection zone = (ConfigurationSection) e.getValue();

                if (
                        !(
                                zone.isInt("x1") &&
                                zone.isInt("x1") &&
                                zone.isInt("y1") &&
                                zone.isInt("z1") &&

                                zone.isInt("x2")  &&
                                zone.isInt("y2")  &&
                                zone.isInt("z2")
                        )
                ) {
                    throw new Exception(String.format("Zone %s at position %s failed to load: broken data", name, i));
                }

                Zone z = new Zone(
                        new Rect(
                                wdk.getServer().getWorld(zone.getString("world")),
                                zone.getInt("x1"),
                                zone.getInt("y1"),
                                zone.getInt("z1"),

                                zone.getInt("x2"),
                                zone.getInt("y2"),
                                zone.getInt("z2")
                                ),
                        null
                        );
                z.setOwners(zone.getStringList("owners"));
                z.setMembers(zone.getStringList("members"));
                z.setRemove(zone.getLong("remove"));

                L.d(String.format("Fetched zone %s at %s", name, z.getRect()));
                zm.push(z, name);
            } catch (Exception exp) {
                brokenData.add(exp);
                L.e(exp.getMessage());
            }
        }
    }

    @Override
    public void saveZonesMap(ZoneManager zm) {
        ConfigurationSection zones = Config.c.createSection("zones");

        for (Map.Entry<Integer, Zone> e : zm.getZones().entrySet()) {
            Zone z = e.getValue();

            ConfigurationSection zone = zones.createSection(z.getName());
            zone.set("x1", z.getRect().getX1());
            zone.set("y1", z.getRect().getY1());
            zone.set("z1", z.getRect().getZ1());

            zone.set("x2", z.getRect().getX2());
            zone.set("y2", z.getRect().getY2());
            zone.set("z2", z.getRect().getZ2());

            zone.set("world", z.getRect().getWorld().getName());
            zone.set("remove", z.getRemove());
            zone.set("owners", z.getOwners());
            zone.set("members", z.getMembers());
        }

        if (brokenData.size() > 0) {
            L.e("Saving impossible: YML contains broken data and needs to be repaired: ");
            for (Exception exp : brokenData) {
                L.e(String.format("  %s", exp));
            }
        } else {
            wdk.saveConfig();
        }
    }
}
