package org.sp.wdk.ui;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.ui.util.CommandDispatcher;
import org.sp.wdk.ui.util.Sender;
import org.sp.wdk.ui.util.VerifyKit;
import org.sp.wdk.world.zone.Zone;

import java.util.ArrayList;

/**
 * User: sp
 * Date: 4/18/13
 * Time: 5:09 AM
 */
public class RemoveDispatcher extends CommandDispatcher {
    public RemoveDispatcher(WorldDreamkit wdk) {
        super(wdk);
        v.add(new VerifyKit() {
            @Override
            public boolean v(CommandSender s, Command c, String str, String[] strings) {
                return strings.length == 1 || s instanceof Player;
            }

            @Override
            public String fail(CommandSender s, Command c, String str, String[] strings) {
                return "You must specify zone name, server!";
            }
        });
    }

    @Override
    public boolean cmd(Sender s, String str, String[] args) {
        Zone z;
        if (args.length == 0) {
            z = wdk.getPm().getZoneFor(s.p());
        } else {
            z = wdk.getZm().find(args[0]);
        }

        if (z == null) {
            s.e(String.format("No zone! (You must specify name or enter zone)", args[0]));
        } else {
            if (!z.ownedBy(s.p()) && !s.p().isOp()) {
                s.e(String.format("You don't own zone %s", args[0]));
            } else {
                z.setRemove(System.currentTimeMillis());
                z.setOwners(new ArrayList<String>());
                s.s(String.format("Zone %s will be removed and regenerated after delay. ", args[0]));
            }
        }
        return false;
    }
}
