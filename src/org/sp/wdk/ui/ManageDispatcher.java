package org.sp.wdk.ui;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.ui.util.CommandDispatcher;
import org.sp.wdk.ui.util.Sender;
import org.sp.wdk.ui.util.VerifyKit;
import org.sp.wdk.world.zone.Zone;

/**
 * User: sp
 * Date: 4/26/13
 * Time: 8:01 PM
 */
public class ManageDispatcher extends CommandDispatcher {
    private WorldDreamkit wdk;
    private static final int C_LIST = 1;
    private static final int C_ADDM = 2;
    private static final int C_ADDO = 3;
    private static final int C_REMOVE = 4;

    class ManageArgs {
        public String player;
        public int sub;
        public Zone z;
    }

    public ManageDispatcher(WorldDreamkit wdk) {
        super(wdk);
        this.wdk = wdk;
        v.add(new VerifyKit() {
            @Override
            public boolean v(CommandSender s, Command c, String str, String[] args) {
                return ! (args.length == 0 || (args[0] == "." && args.length == 1));
            }

            @Override
            public String fail(CommandSender s, Command c, String str, String[] args) {
                return "You must specify zone!";
            }
        });
        v.add(new VerifyKit() {
            @Override
            public boolean v(CommandSender s, Command c, String str, String[] args) {
                return ! (args.length == 2 &&
                        (args[1].equalsIgnoreCase("ao") || args[1].equalsIgnoreCase("am") || args[1].equalsIgnoreCase("rm"))
                );
            }

            @Override
            public String fail(CommandSender s, Command c, String str, String[] args) {
                return "You must specify player name for this commands!";
            }
        });
    }

    public ManageArgs getArgs(Sender s, String[] args) {
        ManageArgs ma = new ManageArgs();
        Zone z;
        int sub = 0;
        String p = null;

        if (args.length == 0 || (args[0].equalsIgnoreCase(".") && args.length == 1)) {
            ma.z = wdk.getPm().getZoneFor(s.p());
            ma.sub = C_LIST;
        } else if (args.length == 1) {
            ma.z = wdk.getZm().find(args[0]);
            ma.sub = C_LIST;
        } else {
            if (args[0].equalsIgnoreCase(".")) {
                ma.z = wdk.getPm().getZoneFor(s.p());
            } else {
                ma.z = wdk.getZm().find(args[0]);
            }

            if (args[1] == "list") {
                ma.sub = C_LIST;
            } else if (args[1].equalsIgnoreCase("am") || args[1].equalsIgnoreCase("addmember")) {
                ma.sub = C_ADDM;
                ma.player = args[2];
            } else if (args[1].equalsIgnoreCase("ao") || args[1].equalsIgnoreCase("addowner")) {
                ma.sub = C_ADDO;
                ma.player = args[2];
            } else if (args[1].equalsIgnoreCase("rm") || args[1].equalsIgnoreCase("remove")) {
                sub = C_REMOVE;
                ma.player = args[2];
            }
        }
        return ma;
    }

    @Override
    public boolean cmd(Sender s, String str, String[] args) {
        ManageArgs ma = getArgs(s, args);
        Zone z = ma.z;
        String p = ma.player;

        if (z == null) {
            s.e(String.format("No zone! (you must specify name or '.' for zone you currently in)"));
        } else if (!z.ownedBy(s.p()) && !s.p().isOp()) {
            s.e(String.format("You don't own zone %s, so you cant manage it! (Maybe you're member?)", z.getName()));
        } else {
            switch (ma.sub) {
                case C_LIST:
                    s.i(String.format("Zone %s:", z.getName()));
                    s.send("    Owners:");
                    for (String member : z.getOwners()) {
                        s.send("        " + member);
                    }
                    s.send("    Members:");
                    for (String member : z.getMembers()) {
                        s.send("        " + member);
                    }
                    break;
                case C_ADDM:
                    if (z.isMember(p)) {
                        s.e(String.format("Player %s is member already!", p));
                    } else {
                        z.addMember(p);
                        s.i(String.format("Added player %s as member to zone %s.", p, z.getName()));
                    }
                    break;
                case C_ADDO:
                    if (z.ownedBy(p)) {
                        s.e(String.format("Player %s is owner already!", p));
                    } else {
                        z.addOwner(p);
                        s.i(String.format("Added player %s as owner to zone %s.", p, z.getName()));
                    }
                    break;
                case C_REMOVE:
                    if (z.ownedBy(p)) {
                        z.removeOwner(p);
                        s.s(String.format("Removed player %s from owners of %s", p, z.getName()));
                    } else if (z.isMember(p)) {
                        z.removeMember(p);
                        s.s(String.format("Removed player %s from members of %s", p, z.getName()));
                    } else {
                        s.e(String.format("Player %s is not member or owner of a %s", p, z.getName()));
                    }
            }
        }

        return false;
    }
}
