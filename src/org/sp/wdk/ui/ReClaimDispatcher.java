package org.sp.wdk.ui;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.ui.util.CommandDispatcher;
import org.sp.wdk.ui.util.Sender;
import org.sp.wdk.ui.util.VerifyKit;
import org.sp.wdk.world.zone.Zone;

/**
 * User: sp
 * Date: 4/18/13
 * Time: 5:28 AM
 */
public class ReClaimDispatcher extends CommandDispatcher {
    public ReClaimDispatcher(WorldDreamkit wdk) {
        super(wdk);
        v.add(new VerifyKit() {
            @Override
            public boolean v(CommandSender s, Command c, String str, String[] strings) {
                return strings.length == 1;
            }

            @Override
            public String fail(CommandSender s, Command c, String str, String[] strings) {
                return "You must specify zone name.";
            }
        });
    }

    @Override
    public boolean cmd(Sender s, String str, String[] args) {
        Zone z;
        if (args.length == 0) {
            z = wdk.getPm().getZoneFor(s.p());
        } else {
            z = wdk.getZm().find(args[0]);
        }
        if (z == null) {
            s.e(String.format("Can't find zone! (specify name or enter zone)", args[0]));
        } else {
            z.setRemove(0);
            z.addOwner(s.p());

            s.s(String.format("Zone %s restored and now belongs to you. ", args[0]));
        }
        return false;
    }
}
