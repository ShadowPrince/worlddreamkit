package org.sp.wdk.ui;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.ui.util.CommandDispatcher;
import org.sp.wdk.ui.util.Sender;
import org.sp.wdk.ui.util.VerifyKit;
import org.sp.wdk.world.zone.Zone;

/**
 * User: sp
 * Date: 4/26/13
 * Time: 7:01 PM
 */
public class SelectDispatcher extends CommandDispatcher {
    public SelectDispatcher(WorldDreamkit wdk) {
        super(wdk);
        v.add(new VerifyKit() {
            @Override
            public boolean v(CommandSender s, Command c, String str, String[] args) {
                return s instanceof Player;
            }

            @Override
            public String fail(CommandSender s, Command c, String str, String[] args) {
                return "You must be player";
            }
        });
    }

    @Override
    public boolean cmd(Sender s, String str, String[] args) {
        Zone z;
        if (args.length == 0) {
            z = wdk.getPm().getZoneFor(s.p());
        } else {
            z = wdk.getZm().find(args[0]);
        }

        if (z == null) {
            s.e(String.format("No zone! (You must specify name or enter it)"));
        } else {
            Selection sel = new CuboidSelection(z.getRect().getWorld(), z.getRect().getStartLocation(), z.getRect().getEndLocation());
            wdk.getWe().setSelection(s.p(), sel);
            s.s("Zone selected!");
        }
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
