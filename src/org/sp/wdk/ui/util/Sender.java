package org.sp.wdk.ui.util;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.sp.wdk.ui.messaging.Msg;

import java.util.logging.Level;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/18/13
 * Time: 2:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class Sender {
    private CommandSender sender;

    public Sender(CommandSender s) {
        this.sender = s;
    }

    public Player p() {
        return (Player) sender;
    }

    public void e(String msg) {
        Msg.e(sender, msg);
    }

    public void i(String msg) {
        Msg.i(sender, msg);
    }

    public void s(String msg) {
        Msg.s(sender, msg);
    }

    public void send(String s) {
        sender.sendMessage(s);
    }
}
