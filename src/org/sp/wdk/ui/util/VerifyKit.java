package org.sp.wdk.ui.util;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/18/13
 * Time: 2:26 AM
 * To change this template use File | Settings | File Templates.
 */
public interface VerifyKit {
    public boolean v(CommandSender s, Command c, String str, String[] args);
    public String fail(CommandSender s, Command c, String str, String[] args);
}
