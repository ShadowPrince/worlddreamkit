package org.sp.wdk.ui.util;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.ui.messaging.Msg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/18/13
 * Time: 2:33 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class CommandDispatcher implements CommandExecutor {
    protected WorldDreamkit wdk;
    protected List<VerifyKit> v = new ArrayList<VerifyKit>();

    public CommandDispatcher(WorldDreamkit wdk) {
        this.wdk = wdk;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
        for (VerifyKit vk : v) {
            if (!vk.v(commandSender, command, s, strings)) {
                new Sender(commandSender).e(vk.fail(commandSender, command, s, strings));
                return false;
            }
        }

        return cmd(new Sender(commandSender), s, strings);
    }

    public abstract boolean cmd(Sender s, String str, String[] args);

    public WorldDreamkit getWdk() {
        return wdk;
    }
}
