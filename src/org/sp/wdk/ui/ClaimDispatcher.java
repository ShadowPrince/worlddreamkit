package org.sp.wdk.ui;

import com.sk89q.worldedit.bukkit.selections.Selection;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.ui.util.CommandDispatcher;
import org.sp.wdk.ui.util.Sender;
import org.sp.wdk.ui.util.VerifyKit;
import org.sp.wdk.world.geom.Rect;
import org.sp.wdk.world.zone.Zone;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/18/13
 * Time: 2:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class ClaimDispatcher extends CommandDispatcher {
    public ClaimDispatcher(WorldDreamkit wdk) {
        super(wdk);
        v.add(new VerifyKit() {
            @Override
            public boolean v(CommandSender s, Command c, String str, String[] strings) {
                return s instanceof Player;
            }

            @Override
            public String fail(CommandSender s, Command c, String str, String[] strings) {
                return "Must be player instance";
            }
        });
        v.add(new VerifyKit() {
            @Override
            public boolean v(CommandSender s, Command c, String str, String[] strings) {
                return getWdk().getWe().getSelection((Player) s) != null;
            }

            @Override
            public String fail(CommandSender s, Command c, String str, String[] strings) {
                return "You must select rectangle";
            }
        });
    }

    @Override
    public boolean cmd(Sender s, String str, String[] args) {
        Selection selection = wdk.getWe().getSelection(s.p());
        if (selection != null) {
            World world = selection.getWorld();
            Location min = selection.getMinimumPoint();
            Location max = selection.getMaximumPoint();

            wdk.getZm().push(new Zone(new Rect(
                    world,
                    min.getX(), min.getY(), min.getZ(),
                    max.getX(), max.getY(), max.getZ()
            ), s.p().getName()));
            s.s("Registered zone");
        } else {
            s.e("No selection");
        }

        return false;
    }
}
