package org.sp.wdk.ui;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.ui.util.CommandDispatcher;
import org.sp.wdk.ui.util.Sender;
import org.sp.wdk.ui.util.VerifyKit;
import org.sp.wdk.world.zone.Zone;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/18/13
 * Time: 2:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class InfoDispatcher extends CommandDispatcher {
    public InfoDispatcher(WorldDreamkit wdk) {
        super(wdk);
        v.add(new VerifyKit() {
            @Override
            public boolean v(CommandSender s, Command c, String str, String[] strings) {
                return s instanceof Player;
            }

            @Override
            public String fail(CommandSender s, Command c, String str, String[] strings) {
                return "You must be player";
            }
        });

    }

    @Override
    public boolean cmd(Sender s, String str, String[] args) {
        Zone z = wdk.getPm().getZoneFor(s.p());
        if (z == null) {
            s.i("No zone here, can't build");
        } else {
            if (z.accessibleBy(s.p())) {
                s.i(String.format("There is %s, accessible to you", z.getName()));
            } else if (z.getRemove() != 0) {
                s.i(String.format(
                        "This zone (%s) marked to remove. If you wanna claim it, type /reclaim. Time last: %dm.",
                        z.getName(),
                        ((1000*60* 10 - (System.currentTimeMillis() - z.getRemove()))/1000)/60
                ));
            } else {
                s.i(String.format("There is zone %s, not accessible to you", z.getName()));
            }
        }
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
