package org.sp.wdk.ui.messaging;

import org.bukkit.command.CommandSender;

import java.util.logging.Level;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/18/13
 * Time: 2:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class Msg {
    public static final String BLACK = "§0";
    public static final String RED = "§c";
    public static final String GREEN = "§2";
    public static final String YELLOW = "§6";
    public static final String CYAN = "§3";
    public static final String WHITE = "§f";
    public static final String RESET = "§r";

    public static void message(Level lvl, CommandSender s, String msg) {
        s.sendMessage(String.format("[%sWDK%s] %s%s] %s", CYAN, RESET, level(lvl), RESET, msg));
    }

    public static String level(Level lvl) {
        switch (lvl.intValue()) {
            case 900: return String.format("[%sE", RED);
            case 800: return String.format("[%sI", YELLOW);
            case 300: return String.format("[%sS", GREEN);
            // 500 - fine
            // -2147483648 - all

            default: return "[?]";
        }
    }

    public static void e(CommandSender s, String msg) {
        message(Level.WARNING, s, msg);
    }

    public static void i(CommandSender s, String msg) {
        message(Level.INFO, s, msg);
    }

    public static void s(CommandSender s, String msg) {
        message(Level.FINEST, s, msg);
    }
}
