package org.sp.wdk.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.listeners.event.PlayerEnterZoneEvent;
import org.sp.wdk.listeners.event.PlayerLeaveZoneEvent;
import org.sp.wdk.log.L;
import org.sp.wdk.ui.util.Sender;

import java.util.EventListener;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 9:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class PlayerListener implements Listener {
    private WorldDreamkit wdk;

    public PlayerListener(WorldDreamkit wdk) {
        this.wdk = wdk;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        L.d(String.format("Player %s quit, pushing to manager.", e.getPlayer().getName()));
        wdk.getPm().eject(e.getPlayer());
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent e) {
        L.d(String.format("Player %s logged, pushing to manager.", e.getPlayer().getName()));
        wdk.getPm().push(e.getPlayer());
    }

    @EventHandler
    public void onPlayerEnterZone(PlayerEnterZoneEvent e) {
        if (e.getZone().accessibleBy(e.getPlayer())) {
            new Sender(e.getPlayer()).i(String.format("Entered %s, accessible to you.", e.getZone().getName()));
        } else {
            new Sender(e.getPlayer()).i(String.format("Entered %s, not accessible to you.", e.getZone().getName()));
        }
    }

    @EventHandler
    public void onPlayerLeaveZone(PlayerLeaveZoneEvent e) {
        new Sender(e.getPlayer()).i(String.format("Leaved %s.", e.getZone().getName()));
    }
}
