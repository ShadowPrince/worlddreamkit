package org.sp.wdk.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerEvent;
import org.sp.wdk.WorldDreamkit;
import org.sp.wdk.log.L;
import org.sp.wdk.ui.util.Sender;
import org.sp.wdk.world.zone.Zone;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 8:25 PM
 * To change this template use File | Settings | File Templates.
 * Bee gees
 */
public class AccessListener implements Listener {
    private WorldDreamkit wdk;
    public AccessListener(WorldDreamkit wdk) {
        this.wdk = wdk;
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (e.getPlayer().isOp()) {
            return;
        }

        Zone z = wdk.getPm().getZoneFor(e.getPlayer());

        if (z != null && z.accessibleBy(e.getPlayer())) {
            L.d(String.format("Player %s try to break block at zone %s. Allowed.", e.getPlayer().getName(), z.getName()));
        } else {
            placeFailed(z == null, e.getPlayer(), z);

            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (e.getPlayer().isOp()) {
            return;
        }

        Zone z = wdk.getPm().getZoneFor(e.getPlayer());

        if (z != null && z.accessibleBy(e.getPlayer())) {
            L.d(String.format("Player %s try to place block at zone %s. Allowed.", e.getPlayer().getName(), z.getName()));
        } else {
            placeFailed(z == null, e.getPlayer(), z);

            e.setCancelled(true);
        }
    }

    public void placeFailed(boolean world, Player p, Zone z) {
        if (world) {
            new Sender(p).e("There is no zone, but you can build only if you claim it!");
            L.d(String.format("Player %s try to operate block at world. Failed.", p.getName()));
        } else {
            new Sender(p).e(String.format("You dont have access to this zone! Owner - %s.", z.getMainOwner()));
            L.d(String.format("Player %s try to operate block at zone %s. Failed.", p.getName(), z.getName()));
        }
    }
}
