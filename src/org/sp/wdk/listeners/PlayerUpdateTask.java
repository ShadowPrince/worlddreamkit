package org.sp.wdk.listeners;

import org.bukkit.scheduler.BukkitRunnable;
import org.sp.wdk.WorldDreamkit;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/17/13
 * Time: 10:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class PlayerUpdateTask extends BukkitRunnable {
    private WorldDreamkit wdk;

    public PlayerUpdateTask(WorldDreamkit wdk) {
        this.wdk = wdk;
    }

    @Override
    public void run() {
        wdk.getPm().update();
    }
}
