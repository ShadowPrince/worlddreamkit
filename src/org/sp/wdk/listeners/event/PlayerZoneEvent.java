package org.sp.wdk.listeners.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.sp.wdk.world.player.PlayerState;
import org.sp.wdk.world.zone.Zone;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/18/13
 * Time: 3:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class PlayerZoneEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private PlayerState ps;

    public PlayerZoneEvent(PlayerState ps) {
        this.ps = ps;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public PlayerState getPlayerState() {
        return ps;
    }

    public Zone getZone() {
        return ps.getZone();
    }

    public Player getPlayer() {
        return ps.getPlayer();
    }
}
