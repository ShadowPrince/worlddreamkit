package org.sp.wdk.config;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.sp.wdk.db.DatabaseInterface;
import org.sp.wdk.log.L;
import org.sp.wdk.world.geom.Rect;
import org.sp.wdk.world.zone.Zone;
import org.sp.wdk.world.zone.ZoneManager;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sp
 * Date: 4/18/13
 * Time: 2:11 AM
 * To change this template use File | Settings | File Templates.
 */
public class Config {
    public static FileConfiguration c;

    public static String s(String k) {
        return c.getString(k);
    }

    public static int i(String k) {
        return c.getInt(k);
    }

    public static Object o(String k) {
        return c.get(k);
    }

}
